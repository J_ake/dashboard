<?php
define("ROOT_PATH", dirname(__FILE__));
ERROR_REPORTING(E_ALL);

require_once 'system/core/core.php';
$Core = new Core();


echo $Core->addCSS("lib/stylesheet.css");
echo $Core->addCSS("//fonts.googleapis.com/css?family=Open+Sans");
echo $Core->addCSS("lib/fontawesome.css");
echo $Core->addJS("lib/js/login.js");
echo $Core->addJS("lib/js/jquery.min.js");

$check = $Core->ipb->getLoginState();

//if(($check["loggedin"] == true)){

  if(isset($_GET["page"])){
    $page = $_GET["page"];
    if(file_exists("system/pages/".$page.".php")){
      require_once("system/pages/".$page.".php");
    }else{
      require_once("system/pages/404.php");
    }
  }else{
    require_once("system/pages/home.php");
  }
/*}else{
  ?>
  <div style='margin: 0 auto; margin-top: 300px; text-align: center; width: 300px;'>
    <img src="lib/img/logo-transp.png"/><br />
    <h3 style="font-family: Open Sans; background: #2C2833; color: #ff8313; padding: 25px 0px; font-weight: 400; font-size: 22px; width: inherit;">Portal coming soon</h3>
  </div>
  <?php
}*/
?>
