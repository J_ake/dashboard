<?php
define("ROOT_PATH", dirname(__FILE__));
ERROR_REPORTING(E_ALL);

require_once 'system/core/core.php';
$Core = new Core();

if(isset($_POST["login-form"])){
  $username = $_POST["username"];
  $password = $_POST["password"];
  if(strlen($username) > 3 && strlen($password) > 4){
    $return = $Core->ipb->loginPlayer($username,$password);
  }
}
 ?>
 <?php if(!isset($return)){
   Header('Location: index.php');
 }else{ ?>
 <!-HTML->
  <html>
    <head>
      <link rel="stylesheet" type="text/css" href="lib/login.css" />
      <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
      <title>Twisted Gamers Portal - Login</title>
    </head>
    <body>
      <div class="login-layer">
        <h1>Twisted Gamers Login</h1>
        <?php
          if($return == true){
            echo "<br /><br /><div class='notify notify-green'>You have successfully logged in! You will get redirected to the portal in 3 seconds</div>";
            Header('Refresh: 3; url=index.php');
          }else{
            echo "<div class='notify notify-red'>Your username or password are incorrect! Please try it again, you will get redirected in 3 seconds</div>";
            echo "<br /><br /><form method='POST' action='login.php'>Username<br /><input type='text' name='username' /><br /><br />Password<br /><input type='password' value='' name='password' /><br /><br /><input type='submit' name='login-form' value='Login' /></form>";
          }
        ?>
      </div>
    </body>
  </html>
  <?php
  }
  ?>
