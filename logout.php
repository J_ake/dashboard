<?php
define("ROOT_PATH", dirname(__FILE__));
ERROR_REPORTING(E_ALL);

require_once 'system/core/core.php';
$Core = new Core();
    $ret = $Core->ipb->logoutPlayer();
    if($ret == true){
      ?>
      <!-HTML->
       <html>
         <head>
           <link rel="stylesheet" type="text/css" href="lib/login.css" />
           <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
           <title>Twisted Gamers Portal - Log</title>
         </head>
         <body>
           <div class="login-layer">
             <h1>Twisted Gamers Logout</h1>
             <?php
                 echo "<br /><br /><div class='notify notify-green'>You have successfully logged out! You will get redirected to the portal in 3 seconds</div>";
                 //Header('Refresh: 5; url=index.php');
             ?>
           </div>
         </body>
       </html>
       <?php
     }
?>
