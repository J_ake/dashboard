<?php
define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT'].'/twistedgamers/portal');
require_once $_SERVER['DOCUMENT_ROOT'].'/twistedgamers/portal/system/core/core.php';

$output_dir = "uploads/";
if(isset($_FILES["myfile"]) && isset($_POST["username"]))
{
	$ret = array();
	$Core = new Core();

//	This is for custom errors;
/*	$custom_error= array();
	$custom_error['jquery-upload-file-error']="File already exists";
	echo json_encode($custom_error);
	die();
*/
	$error =$_FILES["myfile"]["error"];
	$zip = new ZipArchive;

	//You need to handle  both cases
	//If Any browser does not support serializing of multiple files using FormData()
	if(!is_array($_FILES["myfile"]["name"])) //single file
	{
 	 	$fileName = $_FILES["myfile"]["name"]; //fileName
		$fileData = $_FILES["myfile"]["tmp_name"];
		$fileName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileName);

		if($zip->open($fileData) === TRUE){

			// META CHECK //
			if($zip->locateName("meta.xml") != TRUE){
				echo json_encode("nometa");
				$zip->close();
				return false;
			}

			$mapcheck = false;
			for($i = 0; $i < $zip->numFiles; $i++){
				$stat = $zip->statIndex($i);
				if ( strpos( basename($stat["name"]), ".map") ){
					$mapcheck = true;
				}
			}

			// MAP FILE CHECK //
			if(!($mapcheck)){
				echo json_decode(json_encode("nomap"));
				$zip->close();
				return false;
			}

			$zip->extractTo($output_dir.'/'.$fileName);
			$zip->close();

			//SQL//
			$username = $_POST["username"];
			$userid = $_POST["userid"];
			$query = "SELECT mapresource,state FROM `mapuploader` WHERE mapmd5hash = ?";
			$md5hash = hash("md5",$userid.$fileName);
			$stmt = $Core->queryDB($query, array($md5hash));
			if($stmt){
					$Core->execDB(
						"UPDATE mapuploader SET state = ? WHERE mapmd5hash = ?",
						array("reuploaded",$md5hash)
					);
					$ret = "update";
			}else{
				$query = "INSERT INTO `mapuploader` (userid,username,mapresource,state,mapmd5hash) VALUES(?,?,?,?,?)";
				$stmt = $Core->execDB($query, array($userid,$username,$fileName,"untested",$md5hash));
				if($stmt){
					$ret = "ok";
				}else{
					$ret = "failquery";
				}
			}
		}
	}
	else  //Multiple files, file[]
	{
	  $fileCount = count($_FILES["myfile"]["name"]);
	  for($i=0; $i < $fileCount; $i++)
	  {
			$fileName = $_FILES["myfile"]["name"][$i]; //fileName
			$fileData = $_FILES["myfile"]["tmp_name"][$i];
			$fileName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileName);

			if($zip->open($fileData) === TRUE){

				// META CHECK //
				if($zip->locateName("meta.xml") != TRUE){
					echo json_encode("nometa");
					$zip->close();
					return false;
				}

				$mapcheck = false;
				for($i = 0; $i < $zip->numFiles; $i++){
					$stat = $zip->statIndex($i);
					if ( strpos( basename($stat["name"]), ".map") ){
						$mapcheck = true;
					}
				}

				// MAP FILE CHECK //
				if(!($mapcheck)){
					echo json_decode(json_encode("nomap"));
					$zip->close();
					return false;
				}

				$zip->extractTo($output_dir.'/'.$fileName);
				$zip->close();

				//SQL//
				$username = $_POST["username"];
				$userid = $_POST["userid"];
				$query = "SELECT mapresource,state FROM `mapuploader` WHERE mapmd5hash = ?";
				$md5hash = hash("md5",$userid.$fileName);
				$stmt = $Core->queryDB($query, array($md5hash));
				if($stmt){
						$Core->execDB(
							"UPDATE mapuploader SET state = ? WHERE mapmd5hash = ?",
							array("reuploaded",$md5hash)
						);
						$ret = "update";
				}else{
					$query = "INSERT INTO `mapuploader` (userid,username,mapresource,state,mapmd5hash) VALUES(?,?,?,?,?)";
					$stmt = $Core->execDB($query, array($userid,$username,$fileName,"untested",$md5hash));
					if($stmt){
						$ret = "ok";
					}else{
						$ret = "failquery";
					}
				}
			}
	  }

	}
    echo json_encode($ret);
 }

 ?>
