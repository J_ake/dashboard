<html>
	<body>
		{include("lib/navigation.tpl")}

  <div id="main-body">
      <div class="row-100">
				{if $user.loggedin == true}
        <div class="news-box">
          <h1>Benefits</h1>
					<div class="notify notify-blue">
	          <li>$40,000 in-game money for every 1€ you pay</li>
	          <li>1 SkillPoint for every 1€ you pay (PayPal only)</li>
	          <li>Double money and EXP in-game</li>
	          <li>3 free map purchases a day</li>
	          <li>Access to in-game donator panel</li>
	          <li>Special in-game donator group and your own private team chat</li>
	          <li>Special donator group and forum on our forums</li>
	          <li>Unlimited display name changes on forum</li>
	          <li>No limitation on playernick over our dashboard</li>
	          <li>And more!</li>
					</div>
        </div>
				<div class="news-box">
						<h1>Payment Methods</h1>
						<div class="fluid-50">
								<h3>Donate via PayPal</h3>
								<form name="donate_paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
									<h3>Choose amount (EUR)</h3>
									<input type="text" autocomplete="off" id="donate_amount_paypal" value="5" onkeypress="return numericOnly(event)" onkeyup="donationAmountPaypal_changed(this)" />

									<br><br><br><br>
									You will get:
									<ul>
										<li>Status: <span id="pp-benefit-days" style="color:#FF8C00;">25 days</span></li>
										<li>Money: <span id="pp-benefit-money" style="color:#FF8C00;">$250000</span></li>
										<li>SP: <span id="pp-benefit-sp" style="color:#FF8C00;">5</span></li>
									</ul>
									<br><br><br>

									<input type="hidden" name="cmd" value="_xclick">
									<input type="hidden" name="business" value="villevirtala95@gmail.com">
									<input type="hidden" name="receiver_email" value="villevirtala95@gmail.com">
									<input type="hidden" name="item_name" value="Donate to Twisted Gamers">
									<input type="hidden" name="amount" id="paypal_price" value="5">
									<input type="hidden" name="no_shipping" value="1">
									<input type="hidden" name="no_note" value="1">
									<input type="hidden" name="currency_code" value="EUR">
									<input type="hidden" name="rm" value="2">
									<input type="hidden" name="notify_url" value="http://www.twisted-gamers.net/newdonate/assets/ipn_listener.php" />
									<input type="hidden" name="custom" value="{$user.name}">
									<input type="hidden" name="return" value="https://www.twisted-gamers.net/portal/dashboard/donate">
									<input type="hidden" name="cancel_return" value="https://www.twisted-gamers.net/portal/dashboard/donate">
									<input type="submit" id="donatePaypal" class="status status-orange" value="Donate with PayPal!" style="height: 40px; width: 100%; margin-top: -35px; margin-bottom:-10px;" />
								</form>

								{strip js}
								  <script type="text/javascript">
										$(document).ready(function(){
											var allowedNums = new Array(0,1,2,3,4,5,6,7,8,9);
											$("#donate_amount_paypal").keyup(function(){
												var text = $(this).val().replace(/[^0-9\.]/g,'');
												$(this).val(text);
											});
											$("#donate_amount_paypal").on("input",function(e){
												var value = this.value;
												var calc = 4;
												if(/[A-Za-z]/g.test(value)){ return false; }
												value = Number(value);
												value = value > 0 ? value : 0;
												$("#pp-benefit-days").text(Number(value * calc) + ' days');
												$("#pp-benefit-money").text((40000*Number(value)) + '$');
												$("#pp-benefit-sp").text(value + 'SP');
											});
										});
								  </script>
								{/strip}
						</div>
						<div class="fluid-50">
								<h2>Donate by Phone</h2>
								<a id="fmp-button" href="#" rel="c275da0b0a1bf8844a96452abab42873/Jake"><img src="//fortumo.com/images/fmp/fortumopay_150x50_red.png" width="150" height="50" alt="Mobile Payments by Fortumo" border="0" /></a>
							</div>
					</div>
				{else}
				<div class="notify notify-red">
						Please login in order to see this page!
				</div>
				{/if}
      </div>
    </div>
  </body>
</html>
