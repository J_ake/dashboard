<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-70">
				{for i $news}
				<div class="news-box">
						<h1>{$news.$i["title"]} <span class="news-date">{$news.$i["start_date"]}</span></h1>
						{$news.$i["content"]}
				</div>
				{/for}
			</div>
			<div class="row-30">
				{include("lib/viewer.tpl")}
			</div>
		</div>
	</body>
</html>
