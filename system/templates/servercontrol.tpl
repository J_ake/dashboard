<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-100">
				<div class="news-box teamlist-box">
						{if $user["inteam"] && $user["isadmin"]}
						<h1>Twisted Gamers - Serveradmin</h1>
							Please select a server: <select id="room-selector">
								<option value="newserver" selected>MTA:MAIN</option>
								<option value="phoenix">MTA:PHOENIX</option>
								<option value="clanwars">MTA:CLANWARS</option>
							</select>
							<div class="status-console">
								{if $state == 'online'}<span class="status status-big status-orange">Stop server</span> <span class="status status-big status-orange">Restart server</span>{else}<span class="status status-big status-orange">Start server</span>{/if}
							</div>
							<br /><br /><br /><br />
							<h2 class="orange-table">Server-Console {if $state == 'online'}<span class="status status-green" id="server-status" style="float: right;">Server online</span>{else}<span class="status status-red"  id="server-status" style="float: right;">Server offline</span>{/if}</h2>
							<div class="server-console">
								{$console}
							</div>
						{else}
						<div class="notify notify-red">
								You are not allowed to see this page or this page has been disabled!
						</div>
						{/if}
				</div>
			</div>
		</div>
	</body>
</html>
