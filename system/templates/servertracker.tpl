<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-100">
				<div class="news-box teamlist-box">
						<h1>Twisted Gamers - Servertracker</h1>
						{foreach $playerlist["players"] val, key, name='tracker'}
						<h2 class="orange-table">Room: {$playerlist["rooms"][$val]} <span class="status status-lila" style="float: right;">{$playerlist['pcount'][$val]} Players in Room</span></h2>
							<ul class="servertracker servertracker-title">
									<li>Username</li>
									<li>Team</li>
									<li>Playtime</li>
									<li>Money</li>
									<li>County</li>
									<li>ping</li>
							</ul>
								{for i $playerlist["players"][$val]}
								<ul class="servertracker">
										<li>{$playerlist["players"][$val][$i]["username"]}</li>
										<li>{$playerlist["players"][$val][$i].team}</li>
										<li>{$playerlist["players"][$val][$i].playtime}</li>
										<li>{$playerlist["players"][$val][$i].money}</li>
										<li style="text-align: center;"><img src="https://www.twisted-gamers.net/portal/img/flags/{$playerlist['players'][$val][$i].country}.png" class="rounded-image"></img></li>
										<li>{$playerlist["players"][$val][$i].ping}</li>
								</ul>
								{/for}
						{/foreach}
				</div>
			</div>
		</div>
	</body>
</html>
