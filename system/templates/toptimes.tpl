<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-100">
				<div class="news-box">
						<h1>Twisted Gamers - Toptimes</h1>
						<ul class="servertracker servertracker-title orange-table">
							<li style="width: 15%;">Rank</li>
							<li style="width: 55%;">Map</li>
							<li style="width: 15%;">Time</li>
							<li style="width: 15%;">Date</li>
						</ul>
              {for i $toptimes}
              <ul class="servertracker">
                <li style="width: 15%;">{$toptimes[$i]["position"]}</li>
                <li style="width: 55%; text-align: left; padding-left: 5%; padding-right: -5%;">{upper($toptimes[$i]["mapName"])}</li>
                <li style="width: 15%;">{$toptimes[$i]["time"]}</li>
                <li style="width: 15%;"><span class='status status-orange'>{date_format($toptimes[$i]["date"] "%Y-%m-%d")}</span></li>
              </ul>
              {/for}
				</div>
			</div>
		</div>
	</body>
</html>
