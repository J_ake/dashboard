<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-100">
				<div class="news-box">
						<h1>Twisted Gamers - Error 404 / Site not found</h1>
            <div class="notify notify-red">
                Error 404<br /><br />
                Sorry the page you are trying to look for is currently disabled or hasn't been added yet! Please get in contact with Twisted Gamers Administration!<br /><br />
                Return back to <a href="http://localhost/twistedgamers/portalnew">Home</a>
            </div>
        </div>
      </div>
    </div>
  </body>
</html>
