<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-100">
        {if $user.loggedin && $user.inteam}
          <div class="news-box">
              <h1>Banned player list</h1>
              <div id="unban-info" class="" style="display: none;"></div><br />
              {if $data}
                <ul class="servertracker servertracker-title orange-table">
                  <li style="width: 20%;">Banned player</li>
                  <li style="width: 30%;">Ban reason</li>
                  <li style="width: 10%;">Ban date</li>
                  <li style="width: 10%;">Expire date</li>
                  <li style="width: 20%;">Banned by</li>
                  <li style="width: 10%;">Action</li>
                </ul>
                {for i $data}
                  <ul class="servertracker" id="ban-{$i}">
                    <li style="width: 20%;">{$data[$i]["user"]}</li>
                    <li style="width: 30%;">{$data[$i]["reason"]}</li>
                    <li style="width: 10%;">{date_format $data[$i]["time"] "%Y-%m-%d %H:%M"}</li>
                    <li style="width: 10%;">{date_format $data[$i]["unbantime"] "%Y-%m-%d %H:%M"}</li>
                    <li style="width: 20%;">{$data[$i]["admin"]}</li>
                    <li style="width: 10%;"><span class="status status-orange" id="unban-player" data-serial="{$data[$i]['serial']}" data-attribute="{$i}">Unban player</span></li>
                  </ul>
                {/for}
                {literal}
                <script>
                  $(document).ready(function(){
                    $("#unban-player").on("click",function(){
                      var id = $(this).attr("data-attribute");
                      $.ajax({
                        "url": "system/pages/hidden/sendcmd.php",
                        "type": "post",
                        "data": {"banSerial": $(this).attr("data-serial")},
                        "success": function(data){
                            var f = data.indexOf("successfully");
                            if(f > -1){
                              console.log(id);
                              $("#ban-"+id).remove();
                              $("#unban-info").text(data);
                              $("#unban-info").removeClass("notify notify-green notify-red").addClass("notify notify-green");
                              $("#unban-info").fadeIn();
                              setTimeout(function(){
                                $("#unban-info").fadeOut();
                              }, 10000);
                            }else{
                              ("#unban-info").text(data);
                              $("#unban-info").removeClass("notify notify-green notify-red").addClass("notify notify-red");
                              $("#unban-info").fadeIn();
                              setTimeout(function(){
                                $("#unban-info").fadeOut();
                              }, 10000);
                            }
                        }
                      });
                    });
                  });

                </script>
                {/literal}
              {else}
                <div class="notify notify-red">
                    No bans have been found!
                </div>
              {/if}
          </div>
      </div>
  </body>
</html>
