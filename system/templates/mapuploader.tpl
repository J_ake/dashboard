<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-100">
        {if $user.loggedin}
          <div class="news-box">
              <h1>Upload maps</h1>
              <div class="notify notify-blue">
                  <li>Maximum file size is 10MB. Maps exceeding this limit will be rejected</li>
                  <li>Your map should be in a .zip file, with meta.xml and other files in the root of the archive</li>
                  <li>The file should be named according to this pattern: [DM/OS/HDM/FUN/CTF/RACE/SHOOTER/HUNTER]authors-mapname.zip</li>
                  <li>You must be author of the map or have explicit permission from the author to upload the map</li>
                  <li>Select a maximum of 10 maps from "Select maps..."</li>
                  <li>After you have selected your maps, the upload starts automatically</li>
                  <li>Follow further instructions given in the status box</li>
              </div><br /><br />
              <div id="info" style="display: none;">
                All map files have been uploaded correctly and added to the map testing queue
              </div><br />
              <div id="fileuploader">Upload</div>
              {strip js}
              <script type="text/javascript">
              $(document).ready(function() {
              	$("#fileuploader").uploadFile({
              		url: "http://localhost/twistedgamers/portal/system/jq-upload/php/upload.php",
              		fileName: "myfile",
                  multiple: true,
                  allowedTypes: "zip",
                  dragDropStr: "",
                  maxFileCount: 10,
                  dragDrop: false,
                  uploadStr: "Select maps",
                  formData: {
                    'username': {/strip}"{$user.name}"{strip js},
                    'userid': {/strip}"{$user.userid}"{strip js}
                  },
                  afterUploadAll: function(obj)
                  {
                        console.log(obj);
                        var ret = JSON.parse(obj.responses[0]);
                        if(ret == "ok"){
                          $("#info").removeClass("notify notify-red notify-green").addClass("notify notify-green");
                          $("#info").text("All map files have been uploaded correctly and added to the map testing queue");
                          $("#info").show();
                        }
                        if(ret == "nometa"){
                          $("#info").removeClass("notify notify-red notify-green").addClass("notify notify-red");
                          $("#info").text("Upload failed. The meta.xml file is missing. Please check your ZIP-Archive");
                          $("#info").show();
                        }
                        if(ret == "nomap"){
                          $("#info").removeClass("notify notify-red notify-green").addClass("notify notify-red");
                          $("#info").text("Upload failed. The mapfile is missing. Please check your ZIP-Archive");
                          $("#info").show();
                        }
                        if(ret == "failedquery"){
                          $("#info").removeClass("notify notify-red notify-green").addClass("notify notify-red");
                          $("#info").text("Upload failed. A server error occured. Please get in touch with the Twisted Gamers Administration");
                          $("#info").show();
                        }
                        if(ret == "update"){
                          $("#info").removeClass("notify notify-red notify-green").addClass("notify notify-green");
                          $("#info").text("All maps have been uploaded correctly. Some maps might have been updated to a newer version");
                          $("#info").show();
                        }
                  }
              	});
              });
              </script>
              {/strip}
              </body>
            </div>
        {else}
          <div class="news-box">
              <h1>Upload maps</h1>
            <div class='notify notify-red'>[Access Restricted]: Please login to see this page!</div>
          </div>
        {/if}
			</div>
		</div>
	</body>
</html>
