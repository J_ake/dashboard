<div id="main-navigation">
    <!-- User Navigation -->
    <div class="user-navigation">
      {if $user.loggedin == true}
          <li class="user-name">
              Logged in as {$user.name} <i class="fa fa-caret-down"></i>
              <ul class='user-subnav'>
                  <li><a href="?page=stats">Stats</a></li>
                  <li><a href="logout.php">Logout</a></li>
              </ul>
          </li>
      {else}
          Welcome Guest! <a class="login-enable status status-orange" href="#" onClick="showLogin()">Please log in!</a>
          {include ("login.tpl")}
      {/if}
    </div>
    <div class="navigation">
      <a class="tg-logo" href="https://twisted-gamers.net">
        <img src="lib/img/logo-transp.png" width="40px" height="40px"/>
      </a>
      <ul class="dropdown-nav">
          <li>
              <a href="#">Home</a>
              <ul class="dropdown-sub">
                  <li><a href="?page=home">Information & Updates</a></li>
                  <li><a href="?page=donate">Donations</a></li>
                  <li><a href="?page=teamlist">Teamlist</a></li>
                  <li><a href="?page=servertracker">Servertracker</a></li>
              </ul>
          </li>
          <li>
              <a href="#">Stats</a>
              <ul class="dropdown-sub">
                  <li><a href="?page=statstg2">Stats TG 2.0</a></li>
                  <li><a href="?page=stats">Stats</a></li>
                  <li><a href="?page=toptimes">Toptimes</a></li>
                  <li><a href="?page=signature">Signature</a></li>
              </ul>
          </li>
          <li>
              <a href="#">Mapuploader</a>
                <ul class="dropdown-sub">
                  <li><a href="?page=mapuploader">Upload maps</a></li>
                  <li><a href="?page=uploadedmaps">Uploaded maps</a></li>
                </ul>
          </li>
          <li>
              <a href="#">Clan</a>
              <ul class="dropdown-sub">
                  <li><a href="?page=newclan">Register a clan</a></li>
                  <li><a href="?page=myclan">Clan membership</a></li>
                  <li><a href="?page=clanlist">Clans</a></li>
              </ul>
          </li>
          {if $user.inteam}
          <li>
              <a href="#">Webadmin</a>
              <ul class="dropdown-sub">
                  <li><a href="?page=webadmin">Webadmin Panel</a></li>
                  <li><a href="?page=chatlog">Chatlogs</a></li>
                  <li><a href="?page=banlist">Banlist</a></li>
                  {if $user.isadmin}
                  <li><a href="?page=acl">ACL List</a></li>
                  <li><a href="?page=admindonate">Donations</a></li>
                  <li><a href="?page=servercontrol">Server Management</a></li>
                  {/if}
              </ul>
          </li>
          {/if}
      </ul>
    </div>
    <!-- Main Navigation -->
</div>
