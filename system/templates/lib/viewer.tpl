<div class="viewer-box">
    <h1>MTA Server {if $server.mta.state == true}<span class="status status-green">Online</span>{else}<span class="status status-red">Offline</span>{/if}<a href="{$server.mta.joinlink}" class="status status-green" style="float: right; position: relative; margin-right: 5px;">Join the server</a></h1>
    <br /><br />
    Players Online: <span class='status status-orange'>{$server.mta.playercount}/{$server.mta.maxplayers}</span><hr />
    {if $server.mta.players}
    <div class="user-list">
      {for i $server.mta.players}
      <li>{$server.mta.players[$i]}</li>
      {/for}
    </div>
    {/if}
</div>
<div class="viewer-box">
    <h1>Discord Server {if $server.discord.state == true}<span class="status status-green">Online</span>{else}<span class="status status-red">Offline</span>{/if}<a href="{$server.discord.joinlink}" class="status status-green" style="float: right; position: relative; margin-right: 5px;">Join the server</a></h1>
    <div style="text-align: center;"></div><br /><br />
    Players Online <span class='status status-orange'>{$server.discord.playersOnline}</span><hr />
    {if $server.discord.players}
    <div class="user-list">
      {for i $server.discord.players}
      <li>{$server.discord.players[$i]["username"]} {if $server.discord.players[$i]["game"]} <span class='status status-green' style='font-size: 11px; float: right;'>Playing: {$server.discord.players[$i]["game"]}</span>{/if}</li>
      {/for}
    </div>
    {/if}
</div>
<div class="viewer-box">
    <h1>Donations <a href="?page=donate" class='status status-green' style='float: right; margin-right: 5px;'>Donate now</a></h1>
    Reached: <span class='status status-orange'>{$server.donations.target["current"]}&euro; of {$server.donations.target["goal"]}&euro;</span> <br />
    <span style='font-size: 10px;'>Total donations before fees {$server.donations.target["beforeFees"]}&euro;<br />
    <div class="donator-progress">
        <div class="donator-fill" style="width: {$server.donations.target['progress']}%;"></div>
    </div>
    {if $server.donations.latest}
      <hr />
      <h2 style="font-size: 14px; font-weight: 400;">Latest donators</h2>
      <div class="user-list">
          {for i $server.donations.latest}
          <li>{$server.donations.latest.[$i]["name"]} <br /><span class="status status-orange" style="font-size: 11px">{$server.donations.latest[$i]["amount"]}&euro; via {$server.donations.latest[$i]["type"]}</span><span class='status status-green' style="float: right; font-size: 11px">{$server.donations.latest[$i]["date"]}</span></li>
          {/for}
      </div>
    {/if}
</div>
