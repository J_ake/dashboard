<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-100">
				<div class="news-box teamlist-box" style="text-align: center;">
						<h1>Twisted Gamers - Signature</h1>
							{if $user.loggedin}
								Select a signature:
								<select id="room-selector" data-name="{$user.name}" >
										<option value="1" default>1</option>
										<option value="2" >2</option>
										<option value="3" >3</option>
										<option value="4" >4</option>
										<option value="5" >5</option>
										<option value="6" >6</option>
								</select>
								<br /><br />
								<img style="margin: 0 auto;" src="http://localhost/twistedgamers/portalnew/system/signature/signatures/{$user.name}/1.png" id="signature-image" /><br /><br />
								Image URL<br />
								<input type="text" id="signature-link" value="http://localhost/twistedgamers/portalnew/system/signature/signatures/{$user.name}/1.png" readonly /><br /><br />
								HTML URL<br />
								<input type="text" id="signature-html" value="<img src='http://localhost/twistedgamers/portalnew/system/signature/signatures/{$user.name}/1.png' />" readonly /><br /><br />
							{else}
							<div class='notify notify-red'>
									Sorry we were unable to locate your old Account Data. Please contact the Twisted Gamers Administration
							</div>
							{/if}
				</div>
			</div>
		</div>
	</body>
</html>
