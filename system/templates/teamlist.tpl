<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-100">
				<div class="news-box teamlist-box">
						<h1>Twisted Gamers - Teamlist</h1>
						<table class='teamlist'>
              <thead cellpading="0" cellspacing="0">
                  <tr>
                    <td>Forum Name</td>
										<td>Country</td>
                    <td>Member Role</td>
										<td>Ingame Name</td>
										<td>Ingame Status</td>
                  </tr>
              </thead>
              <tbody>
              {for i $team}
              <tr>
                <td><img src="{$team[$i]["avatar"]}" style="width: 32px; height: 32px; border-radius: 50%;" /> <a style="color: #fff;" href="https://forum.twisted-gamers.net/profile/{$team[$i]['userid']}-{$team[$i]['name']}">{$team[$i]["name"]}</a></td>
								<td style="text-align: center;"><img src="https://www.twisted-gamers.net/portal/country/{lower($team[$i]['country'])}" class='rounded-image-team' /></td>
                <td style="text-align: center;">{$team[$i]["title"]}</td>
								<td style="text-align: center;">{$team[$i]["mtanick"]}</td>
								<td style="text-align: center;">{if $team[$i]["online"] == 1}<span class='status status-green'>Online</span>{else}<span class='status status-red'>Offline</span>{/if}</td>
              </tr>
              {/for}
            </table>
				</div>
			</div>
		</div>
	</body>
</html>
