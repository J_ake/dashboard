<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-100">
				<div class="news-box teamlist-box">
						<h1>Twisted Gamers - Statistics {if $type == 'old'}Old{else}New{/if}</h1>
							{if $data && $type == 'old'}
							<div class="fluid-100">
								<table class="teamlist">
									<thead>
											<tr>
													<td width="50%" style="text-align: center;">Stats</td>
													<td width="50%">Value</td>
											</tr>
									</thead>
		              <tbody>
									{foreach $data key, value name='default'}
		              <tr>
		                <td class='first-fix'>{$key}</td>
		                <td style="text-align: center;">{$data[$key]}</td>
		              </tr>
									{/foreach}
								</tbody>
		            </table>
							</div>
							{elseif $data && $type == 'new'}
								<!-- Normal stats -->
								<div class="fluid-70">
									<h2 class="orange-table">Overall Statistics</h2>
									{foreach $data["stats"] key, value name='default'}
									<ul class="servertracker ul-50">
										<li>{$key}</li>
										<li>{$data["stats"][$key]}</li>
									</ul>
									{/foreach}
									{foreach $data["rooms"] key, value}
										<h2 class="orange-table">Statistics for Room {upper($key)}</h2>
										{foreach $data["rooms"][$key] newkey, value}
											<ul class="servertracker ul-50">
												<li>{$newkey}</li>
												<li>{$value}</li>
											</ul>
										{/foreach}
									{/foreach}
								</div>
								<div class="fluid-30">
									<div style="position: fixed; width: 25%;">
									<h2 class="orange-table">Maps played in room</h2>
										<canvas id="userchart" width="80%" height="80%"></canvas>
										{strip js}
										<script type="text/javascript">
											var ctx = document.getElementById("userchart");
											Chart.defaults.global.defaultFontColor = 'white';
											Chart.defaults.global.defaultFontFamily = 'Open Sans';
											var myChart = new Chart(ctx, {
											    type: 'doughnut',
											    data: {
											        labels: ["DM", "HDM", "OS", "DD", "Shooter", "CTF"],
											        datasets: [{
											            label: '# of Votes',
											            data: [ {$data["chart"]["dm"]["mapsplayed"]}, {$data["chart"]["hdm"]["mapsplayed"]}, {$data["chart"]["os"]["mapsplayed"]}, {$data["chart"]["dd"]["mapsplayed"]}, {$data["chart"]["shooter"]["mapsplayed"]}, {$data["chart"]["ctf"]["mapsplayed"]} ],
											            backgroundColor: [
											                'rgba(255, 99, 132, .8)',
											                'rgba(54, 162, 235, .8)',
											                'rgba(255, 206, 86, .8)',
											                'rgba(75, 192, 192, .8)',
											                'rgba(153, 102, 255, .8)',
											                'rgba(255, 159, 64, .8)'
											            ],
											            borderWidth: 0
											        }]
											    }
											});
											</script>
											{/strip}
										</div>
								</div>
							{else}
							<div class='notify notify-red'>
									Sorry we were unable to locate your old Account Data. Please contact the Twisted Gamers Administration
							</div>
							{/if}
				</div>
			</div>
		</div>
	</body>
</html>
