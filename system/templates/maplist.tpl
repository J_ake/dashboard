<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-100">
        {if $user.loggedin}
          <div class="news-box">
              <h1>Uploaded maps</h1>
              {if $maplist}
                <table>
                  <thead>
                      <tr>
                        <td>Mapname</td>
                        <td>Uploaded by</td>
                        <td>Comment</td>
                        <td>Tester</td>
                        <td>State</td>
                        <td>Uploaded on</td>
                      </tr>
                  </thead>
                  <tbody>
                  {for i $maplist}
                    <tr>
                        <td>{$maplist[$i]["mapresource"]}</td>
                        <td style="text-align: center;"><a href="https://forum.twisted-gamers.net/profile/{$maplist[$i]['userid']}-{$maplist[$i]['username']}">{$maplist[$i]["username"]}</a></td>
                        <td>{$maplist[$i]["comment"]}</td>
                        <td>{$maplist[$i]["tester"]}</td>
                        <td style="text-align: center;">
                          {if $maplist[$i]["state"] == "untested" || $maplist[$i]["state"] == "reuploaded"}
                            <span class="status status-orange">{$maplist[$i]["state"]}</span>
                          {elseif $maplist[$i]["state"] == "denied"}
                            <span class="status status-red">{$maplist[$i]["state"]}</span>
                          {elseif $maplist[$i]["state"] == "accepted"}
                            <span class="status status-green">{$maplist[$i]["state"]}</span>
                          {/if}
                        </td>
                        <td style="text-align: center;"><span class="status status-blue">{$maplist[$i]["date"]}</span></td>
                    </tr>
                  {/for}
                  </tbody>
                </table>
              {/if}
            </div>
          {else}
            <div class="news-box">
              <h1>Uploaded maps</h1>
              <div class='notify notify-red'>[Access Restricted]: Please login to see this page!</div>
            </div>
          {/if}
      </div>
    </div>
  </body>
</html>
