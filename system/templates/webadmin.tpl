<html>
	<body>
		{include("lib/navigation.tpl")}
		<div id="main-body">
			<div class="row-100">
				<div class="news-box teamlist-box">
						{if $user["inteam"]}
						<h1>Twisted Gamers - Webadministration</h1>
							<!--Room Selector-->
							Select a room:
							<select name="room-selection" id="room-selector">
									<option value="all"  selected>All</option>
									<option value="bar">Bar</option>
									<option value="dm">Deathmatch</option>
									<option value="hdm">Hard Deathmatch</option>
									<option value="os">Oldschool</option>
									<option value="dd">Destruction Derby</option>
									<option value="shooter">Shooter</option>
									<option value="ctf">Capture the flag</option>
									<option value="fun">Fun</option>
									<option value="training">Training</option>
							</select><br /><br />
							<div class="notify notify-blue">
									Valid commands:<br />
									/ban [playerName] [reason] [duration in hours]<br />
									/kick [playerName] [reason]<br />
									/mute [playerName] [reason] [duration in minutes]<br />
									/reconnect [playerName] [reason]<br />
									for chatting just write something. e. g. "Hi my Name is Jake!"<br />
							</div><br />
							<div class="notify" style="display: none;" id="cmd-info"></div><br />
							<div class="webadmin-useraction" id="interaction">
									<span id="user-cc"></span>
									<span class="status status-orange" id="kick">Kick</span>
									<span class="status status-orange" id="ban">Ban</span>
									<span class="status status-orange" id="reconnect">Force reconnect</span>
									<span class="status status-orange" id="mute">Mute</span>
							</div>
							<input type="text" value="" style="width: 90.5%;" user-name="{$user.name}" id="mta-cmd" />
							<button id="mta-cmd-btn" class="status status-orange" style="border-radius: 0 !important; width: calc(10% - 20px); padding: 6px; 10px;">Send Command</button><br /><br />
							<ul id="mta-message" ></ul>
							<ul id="mta-players" class="webadmin-list">
							</ul>
							{literal}
								<script>
									$(document).ready(function(){

										$("#room-selector").on("change",function(){
												$("#mta-message").load("system/pages/hidden/loadchat.php?room="+$(this).val());
										});

										triggerFunc = function(event){
											if(event.target){
												var e = event.target || event;
												var data = e.getAttribute("data-id");;
												var val = parseHex(data);
												$("#user-cc").attr("user-name",data);
												$("#user-cc").text("Selected Player: "+val);
												$("#interaction").fadeIn();
											}
										}

										$("#kick").on("click",function(){
											$("#mta-cmd").val("/kick "+$("#user-cc").attr("user-name"));
										});
										$("#ban").on("click",function(){
											$("#mta-cmd").val("/ban "+$("#user-cc").attr("user-name"));
										});
										$("#reconnect").on("click",function(){
											$("#mta-cmd").val("/reconnect "+$("#user-cc").attr("user-name"));
										});
										$("#mute").on("click",function(){
											$("#mta-cmd").val("/mute "+$("#user-cc").attr("user-name"));
										});

										function parseHex(a){
											if(/#[A-Fa-f0-9]{6}/g.test(a)){
												a = a.replace(/#[A-Fa-f0-9]{6}/g, "");
											}
											return a;
										}

										$("#mta-cmd-btn").on("click",function(){
												if(!($("#mta-cmd").val())){return;}
												$.ajax({
													url: "system/pages/hidden/sendcmd.php",
													data: {"cmd": $("#mta-cmd").val(), "authkey": "tg7532", "user": $("#mta-cmd").attr("user-name"), "room": $("#room-selector").val()},
													type: "post",
													success: function(data){
														var find = data.indexOf("successfully");
														if(find > -1){
															$("#cmd-info").text(data);
															$("#cmd-info").removeClass("notify notify-green notify-red").addClass("notify notify-green");
															$("#cmd-info").fadeIn();
															setTimeout(function(){
																$("#cmd-info").fadeOut();
															}, 10000);
														}else{
															$("#cmd-info").text(data);
															$("#cmd-info").removeClass("notify notify-green notify-red").addClass("notify notify-red");
															$("#cmd-info").fadeIn();
															setTimeout(function(){
																$("#cmd-info").fadeOut();
															}, 10000);
														}
													},
													error: function(){
														console.log("FAILURE");
													}
												});
										});

										function reloadChat(){
											$("#mta-message").load("system/pages/hidden/loadchat.php?room="+$("#room-selector").val());
										}

										function reloadPlayers(){
											$("#mta-players").load("system/pages/hidden/sendcmd.php?playerlist=true");
										}

										reloadChat();
										reloadPlayers();

										setInterval(function(){
											reloadChat();
											reloadPlayers();
										}, 5000);
									})
								</script>
							{/literal}
						{else}
						<div class="notify notify-red">
								You are not allowed to see this page or this page has been disabled!
						</div>
						{/if}
				</div>
			</div>
		</div>
	</body>
</html>
