<?php
  require_once('../../core/database.class.php');

  class loadChat{

    private $dbHandle;

    public function __construct(){
      $this->runDatabase();
    }

    private function runDatabase(){

      $this->dbHandle = new Database(
        "mysql:host=37.59.53.223;dbname=phoenix",
        "phoenix",
        "DKiLhM35LtD0CeL1"
      );

    }

    public function loadEntries($max, $room = "all"){
      if($room == "all"){
        $query = "SELECT player, room, message FROM chat ORDER BY id DESC LIMIT 50";
        $stmt = $this->dbHandle->querySafe($query);
      }else{
        $query = "SELECT * FROM chat WHERE room = ? ORDER BY id DESC LIMIT 50";
        $stmt = $this->dbHandle->querySafe($query,array($room));
      }
      $ret = "";
      if($stmt && count($stmt) > 0){
        for($i = 0; $i < count($stmt); $i++){
          $ret .= "<li><span style='color: #ff8313;'>(Room)[".$stmt[$i]["room"]."]</span> ".$this->renderColorCodes($stmt[$i]["player"]).": ".$stmt[$i]["message"]."</li>";
        }
      }
      echo $ret;

    }

    private function renderColorCodes($name, $dark = true)
  	{
  	    if(empty($name))
  	    {
  	        return "None found";
  	    }
  	    $name = htmlspecialchars($name);
  	    if(preg_match('/^(#[0-9a-fA-F]{6})+$/', $name) === 1)
  	    {
  	        return $name;
  	    }
  	    preg_match_all('/#[0-9a-fA-F]{6}/', $name, $codes);
  	    $replaced = array();
  	    $codes_original = $codes;
  	    $i = 0;
  	    $count = 1;
  	    $darkify = $dark ? 127 : 0;
  	    foreach($codes[0] as &$code)
  	    {
  	        if(in_array($codes_original[0][$i], $replaced))
  	        {
  	            continue;
  	        }
  	        $code = sprintf('%02s', dechex((hexdec($code[1].$code[2])/255*128 + $darkify)))
  	            .sprintf('%02s', dechex((hexdec($code[3].$code[4])/255*128 + $darkify)))
  	            .sprintf('%02s', dechex((hexdec($code[5].$code[6])/255*128 + $darkify)));
  	        $name = str_replace($codes_original[0][$i], "<span style=\"color: #$code;\">", $name, $count);
  	        $replaced[] = $codes_original[0][$i];
  	        $i++;
  	        $count = 1;
  	    }
  	    while($i > 0)
  	    {
  	        $name .= "</span>";
  	        $i--;
  	    }
  	    return $name;
  	}


  }


  $_room = isset($_GET["room"]) ? $_GET["room"] : "all";
  $chat = new loadChat();
  $chat->loadEntries(50,$_room);

?>
