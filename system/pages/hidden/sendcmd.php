<?php
    define('ROOT_PATH','../../..');
    require_once('../../core/mtauser.class.php');

    class CMD{

      private $mta;
      private $validCMD;

      public function __construct(){

        $this->mta = new MTAUser();
        $this->validCMD = array('kick','ban','reconnect');

      }

      public function sendCMD($cmd,$user,$room){
        if(!strpos($cmd,'ban')&& !strpos($cmd,'kick') && !strpos($cmd,'reconnect') && !strpos($cmd,'mute')){
         return $this->mta->callCMD(
              array(
                "type" => "say",
                "message" => $cmd,
                'user' => $user,
                'room' => $room
              )
          );
        }
        //Ban//
        if(strpos($cmd,'ban')){
          $_first = substr($cmd,0,4);
          $cmd = substr($cmd, 4, strlen($cmd));
          $_second = preg_split('/\s+/', $cmd);
          if(count($_second) > 4){
            $cache = "";
            for($i = 2; $i < (count($_second)-1); $i++){
              $cache .= " ".$_second[$i];
            }
            $_second[2] = $cache;
          }
          return $this->mta->callCMD(
            array(
              "type" => "ban",
              "target" => $_second[1],
              "reason" => $_second[2],
              "duration" => floatval($_second[count($_second)-1]),
              "user" => $user
            )
          );
        }
        //KICK//
        if(strpos($cmd,'kick')){
          $_first = substr($cmd,0,4);
          $cmd = substr($cmd, 4, strlen($cmd));
          $_second = preg_split('/\s+/', $cmd);
          if(count($_second) > 3){
            $cache = "";
            for($i = 2; $i < (count($_second)); $i++){
              $cache .= " ".$_second[$i];
            }
            $_second[2] = $cache;
          }
          return $this->mta->callCMD(
            array(
              "type" => "kick",
              "target" => $_second[1],
              "reason" => $_second[2],
              "user" => $user
            )
          );
        }
        //MUTE//
        if(strpos($cmd,'mute')){
          $_first = substr($cmd,0,4);
          $cmd = substr($cmd, 4, strlen($cmd));
          $_second = preg_split('/\s+/', $cmd);
          if(count($_second) > 4){
            $cache = "";
            for($i = 2; $i < (count($_second)-1); $i++){
              $cache .= " ".$_second[$i];
            }
            $_second[2] = $cache;
          }
          return $this->mta->callCMD(
            array(
              "type" => "mute",
              "target" => $_second[1],
              "reason" => $_second[2],
              "duration" => floatval($_second[count($_second)-1]),
              "user" => $user
            )
          );
        }
        if(strpos($cmd,'reconnect')){
          $_first = substr($cmd,0,9);
          $_second = preg_split('/\s+/', $cmd);
          return $this->mta->callCMD(
            array(
              "type" => "reconnect",
              "target" => $_second[1],
              "user" => $user
            )
          );
        }
      }

      public function getList($room){
        $str = "";
        $ret = $this->mta->getAllPlayers();
        if(count($ret) > 0){
          for($i = 0; $i < count($ret); $i++){
            $str .= '<li id="'.$ret[$i]["username"].'">'.$ret[$i]["user"].' <span id="player-option-btn" onClick="triggerFunc(event)" style="width: 70px; float: right; text-align: center;" class="status status-orange" data-id="'.$ret[$i]['username'].'">options</span></li>';
          }
        }
        return $str;
      }

      public function removeBan($serial){
        return $this->mta->unban($serial);
      }

    }

    $req = isset($_POST["cmd"]) ? $_POST["cmd"] : false;
    $auth_key = isset($_POST["authkey"]) ? $_POST["authkey"] : false;
    $user = isset($_POST["user"]) ? $_POST["user"] : false;
    $room = isset($_POST["room"]) ? $_POST["room"] : false;

    if($req && ($auth_key == 'tg7532') && $user && $room){
      $chat = new CMD();
      $ret = $chat->sendCMD($req,$user,$room);
      echo $ret[0];
      return;
    }

    // Unban //
    $unban = isset($_POST["banSerial"]) ? $_POST["banSerial"] : false;
    if($unban){
      $chat = new CMD();
      $ret = $chat->removeBan($unban);
      echo $ret[0];
      return;
    }

    //Playerlist//
    $list = isset($_GET["playerlist"]) ? $_GET["playerlist"] : false;
    if ($list){
      $chat = new CMD();
      $ret = $chat->getList($list);
      echo $ret;
    }

?>
