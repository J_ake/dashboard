<?php

  $userData = $Core->ipb->getLoginState();
  if($userData["name"]){
    $storageData = $Core->ipb->storage->loadUserDataOld($userData["name"]);
  }

  $data = array(
    'user' => $userData,
    'data' => $storageData,
    'type' => 'old'
  );

  echo $Core->loadTemplate($data,'stats.tpl');

?>
