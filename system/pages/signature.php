<?php
  require_once('system/signature/signature.php');

  $data = array(
    'user' => $Core->ipb->getLoginState(),
  );

  for($i = 1; $i < 7; $i++){
    newSignature($data["user"]["name"],$i);
  }

  echo $Core->loadTemplate($data,'signature.tpl');
  echo $Core->addJS("lib/js/signature.js");
?>
