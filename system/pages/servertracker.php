<?php

  require_once ROOT_PATH.'/system/core/mtauser.class.php';

  $mtaviewer = new MTAUser();

  $data = array(
    "user" => $Core->ipb->getLoginState(),
    "playerlist" => $mtaviewer->loadPlayerList()
  );

  echo $Core->loadTemplate($data,"servertracker.tpl");

?>
