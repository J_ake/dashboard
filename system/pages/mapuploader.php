<?php

    $data = array(
      "user" => $Core->ipb->getLoginState()
    );

    echo $Core->addCSS("system/jq-upload/css/uploadfile.custom.css");
    echo $Core->addJS("lib/js/jquery.min.js");
    echo $Core->addJS("system/jq-upload/js/jquery.uploadfile.js");

    echo $Core->loadTemplate($data,  "mapuploader.tpl");
?>
