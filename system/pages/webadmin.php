<?php
  /**
    @Copyright 2017 Twisted Gamers
    @Author: Jake
    @Version: 0.0.1
  **/

  $data = array(
    "user" => $Core->ipb->getLoginState()
  );

  echo $Core->loadTemplate($data,"webadmin.tpl");
?>
