<?php

  $userData = $Core->ipb->getLoginState();
  if($userData["name"]){
    $serial = $Core->ipb->storage->getPlayerSerial($userData["name"]);
    $ttData = $Core->ipb->storage->loadToptimesOld($serial["lastSerial"]);
  }

  $data = array(
    'user' => $userData,
    'toptimes' => $ttData
  );

  echo $Core->loadTemplate($data,'toptimes.tpl');

?>
