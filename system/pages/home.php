<?php

    $data = array(
      "news" => $Core->ipb->loadNews(),
      "user" => $Core->ipb->getLoginState(),
      "server" => array(
        "mta" => $Core->getMTAStatus(),
        "discord" => $Core->getDiscordStatus(),
        "donations" => array(
          "latest" => $Core->ipb->getLatestDonators(),
          "target" => $Core->ipb->getMonthlyDonationGoal()
        )
      )
    );

    echo $Core->loadTemplate($data,  "home.tpl");
?>
