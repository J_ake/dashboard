<?php

  require_once 'system/core/ssh.class.php';

  $server = new ServerSSH('phoenix','phoenix');

  $data = array(
    "user" => $Core->ipb->getLoginState(),
    "state" => $server->getStatus(),
    "console" => $server->getConsole()
  );
  
  echo $Core->loadTemplate($data,"servercontrol.tpl");

?>
