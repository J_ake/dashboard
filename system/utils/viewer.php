<?php

//Include GameQ Class//
require_once ROOT_PATH.'/system/utils/GameQ/Autoloader.php';
require_once ROOT_PATH.'/system/utils/discord.php';

class Viewer{

  private $pID;
  private $dir;
  private $ssh;
  private $mtaArray;
  private $viewer;
  private $discord;

  public function __construct(){
    $this->checkServerStatus();
    $this->checkDiscord();
    //$this->connectShell();
  }

  private function connectShell(){
    $this->ssh = ssh2_connect('twisted-gamers.net', 22);
    ss2_auth_passowrd($this->ssh, 'tg-mta', 'safetyfirst21');
    $this->dir = 'home/server/MTA/newserver';

    $this->status = $this->checkStatus();

  }

  protected function checkServerStatus(){
    $this->viewer = new GameQ\GameQ();
    $this->viewer->addServers(
      array(
         array(
           "id" => "Twisted Gamers",
          "type" => "mta",
          "host" => "37.59.53.223:22003"
          )
        )
    );
    $this->viewer->setOption('timeout', 3);
    $this->viewer->addFilter('normalise');
    $result = $this->viewer->process()["Twisted Gamers"];
    $ptable = $result["players"];
    $pArray = array();
    for($i = 0; $i < count($ptable); $i++){
      $pArray[] = $ptable[$i]["name"];
    }
    $this->mtaArray = array(
      "joinlink" => $result["gq_joinlink"],
      "servername" => $result["gq_hostname"],
      "playercount" => $result["gq_numplayers"],
      "state" => $result["gq_online"],
      "maxplayers" => $result["max_players"],
      "players" => $pArray
    );
  }

  public function getMTAStatus(){
    return $this->mtaArray;
  }

  private function checkDiscord(){
    $this->discord = new Discord();
  }

  public function getDiscordData(){
    return $this->discord->getDiscordData();
  }

}

?>
