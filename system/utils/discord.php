<?php


  Class Discord{

    private $id;
    private $data;

    public function __construct(){
      $this->id = "215206430037377024";
      $this->retrieveData();
    }

    private function retrieveData(){
      $uri = "https://discordapp.com/api/servers/".$this->id."/widget.json";
      $contents = json_decode(file_get_contents($uri), true);
      if($contents != NULL){
        $this->convertData($contents);
      }
    }

    private function convertData($a){
      $playerData = array();
      for($i = 0; $i < count($a["members"]); $i++){
        $game = false;
        if(isset($a["members"][$i]["game"])){
          $game = $a["members"][$i]["game"]["name"];
        }
        $playerData[$i] = array(
          "username" => $a["members"][$i]["username"],
          "game" => $game
        );
      }
      $this->data = array(
        "playersOnline" => count($a["members"]),
        "joinlink" => $a["instant_invite"],
        "players" => $playerData,
        "state" => true
      );
    }

    public function getDiscordData(){
      return $this->data;
    }

  }

  ?>
