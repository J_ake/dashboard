<?php

require_once ROOT_PATH.'/system/utils/mta_sdk.php';

class MTAUser{

	private $players;
	private $userData;
	private $handle;
	private $resHandle;


	public function __construct(){
		$this->handle = new mta("127.0.0.1", 22005, 'jake', '1234');
		if($this->handle){
			$this->resHandle = $this->handle->getResource('portal');
			$this->definePredefined();
		}else{
			return false;
		}
	}

	public function loadPlayerList(){
		if($this->resHandle){
			$this->players = array(
					"bar" => array(),
					"dm" => array(),
					"hdm" => array(),
					"oldschool" => array(),
					"race" => array(),
					"shooter" => array(),
					"training" => array(),
					"dd" => array(),
					"fun" => array()
			);
			$this->roomCount = array(
					"bar" => 0,
					"dm" => 0,
					"hdm" => 0,
					"oldschool" => 0,
					"race" => 0,
					"traning" => 0,
					"shooter" => 0,
					"dd" => 0,
					"fun" => 0
			);
			$ret = $this->resHandle->call("getPlayerList");
			if($ret and count($ret) > 0){
				$ret = $ret[0];
				for($i = 0; $i < count($ret); $i++){
					$teamDisplay = "<span class='status status-blue'>Players</span>";
					switch($ret[$i]["team"]){
						case "tg":
							$teamDisplay = "<span class='status status-orange'>Twisted Gamers</span>";
							break;
						case "donator":
							$teamDisplay = "<span class='status status-green'>Donators</span>";
							break;
					}
					$this->players[$ret[$i]["room"]][] = array(
						"username" => $this->renderColorCodes($ret[$i]["username"]),
						"playtime" => $this->convertPlaytime($ret[$i]["playtime"]),
						"money" => $ret[$i]["money"].'$',
						"country" => $ret[$i]["country"],
						"ping" => $ret[$i]["ping"],
						"team" => $teamDisplay
					);
					$this->roomCount[$ret[$i]["room"]]++;
				}
				$order = array("<span class='status status-blue'>Players</span>", "<span class='status status-green'>Donators</span>", "<span class='status status-orange'>Twisted Gamers</span>");
				foreach($this->players as $key => $value){
					usort($this->players[$key], function($a, $b) use ($order){
						$_a = array_search($a["team"],$order);
						$_b = array_search($b["team"], $order);
						return $_a - $_b;
					});
				}
				$roomNames = array(
					'bar' => 'Bar',
					'dm' => 'Deathmatch',
					'hdm' => 'Hard Deathmatch',
					'oldschool' => 'Oldschool',
					'race' => 'Race',
					'training' => 'Training',
					'dd' => 'Destruction Derby',
					'fun' => 'Fun',
					'shooter' => 'Shooter'
 				);
				return array('players' => $this->players, 'rooms' => $roomNames, 'pcount' => $this->roomCount);
			}
		}
	}

	private function definePredefined(){
		$this->statsKeys = array(
			"timesjoined" => "Times joined",
			"deaths" => "Deaths",
			"winstreak" => "Highest Winstreak",
			"exp" => "EXP",
			"money" => "Money",
			"playtime" => "Playtime",
			"wins" => "Maps won",
			"mapsplayed" => "Total maps played"
		);
		$this->newRoomKeys = array(
			'dm' => array(
				'mapsplayed' => 'DM maps played',
				'toptimes' => 'Toptimes',
				'mapswon' => 'DM maps won',
				'hunters' => 'Hunters taken'
			),
			'os' => array(
				'mapsplayed' => 'OS maps played',
				'toptimes' => 'Toptimes',
				'mapswon' => 'OS maps won',
				'hunters' => 'Hunters taken'
			),
			'hdm' => array(
				'mapsplayed' => 'HDM maps played',
				'toptimes' => 'Toptimes',
				'mapswon' => 'HDM maps won',
				'hunters' => 'Hunters taken'
			),
			'dd' => array(
				'kills' => 'Kills',
				'mostroundkills' => 'Most kills in a round',
				'mapswon' => 'DM maps won',
				'mapsplayed' => 'DD maps played'
			),
			'shooter' => array(
				'rocketsfired' => 'Rockets fired',
				'kills' => 'Kills',
				'mapswon' => 'DM maps won',
				'mapsplayed' => 'Shooter maps played'
			),
			'ctf' => array(
				'flagswon' => 'Flags captured',
				'flagsreturned' => 'Flags returned',
				'mapswon' => 'CTF maps won',
				'mapsplayed' => 'CTF maps played'
			)
		);
	}

	public function callCMD($a){

		return $this->resHandle->call("webCallFunc",json_encode($a));

	}

	public function getBans(){
		$return = $this->resHandle->call("loadBans");
		if($return && (count($return) > 0)){
			$return = $return[0];
			for($i = 0; $i < count($return); $i++){
				$return[$i]["user"] = $this->renderColorCodes($return[$i]["user"]);
			}
			return $return;
		}
		return false;
	}

	public function getAllPlayers(){
		$return = $this->resHandle->call("getAllPlayers");
		if($return && (count($return) > 0)){
			$return = $return[0];
			for($i = 0; $i < count($return); $i++){
				$return[$i]["username"] = $return[$i]["user"];
				$return[$i]["user"] = $this->renderColorCodes($return[$i]["user"]);
			}
			return $return;
		}
		return false;
	}

	public function unban($serial){
		return $this->resHandle->call("unban",$serial);
	}

	public function getAccountData($userName,$userPass){
		$ret = $this->resHandle->call("getPlayerAccountData",$userName,$userPass);
		if($ret[0] != "not found"){
			$retTable = array(
				'stats' => array(),
				'rooms' => array(
					'dm' => array(), 'dd' => array(), 'hdm' => array(), 'os' => array(), 'ctf' => array(), 'shooter' => array()
				),
				"chart" => array(
					'dm' => array(), 'dd' => array(), 'hdm' => array(), 'os' => array(), 'ctf' => array(), 'shooter' => array()
				));
			$ret = $ret[0];
			//Convert user Stats //
			foreach($ret["stats"] as $key => $value){
				if(array_key_exists($key, $this->statsKeys)){
					if($key == "playtime"){ $value = $this->convertPlayTime($value); }
					if($key == "money"){ $value = '$'.' '.number_format(floatval($value),0,',','.');}
					$retTable["stats"][$this->statsKeys[$key]] = $value;
				}
			}
			//Convert room stats //
			foreach($ret["room"] as $key => $val){
				foreach($ret["room"][$key] as $_key => $value){
					if(array_key_exists($_key, $this->newRoomKeys[$key])){
						$retTable["rooms"][$key][$this->newRoomKeys[$key][$_key]] = $value;
						$retTable["chart"][$key][$_key] = $value;
					}
				}
			}
			return $retTable;
		}else{
			return array('data' => 'not found');
		}
	}

	private function convertPlayTime($time){
		$hours = floor($time/60);
		$minutes = floor($time - ($hours*60));
		return sprintf('%02d',$hours).':'.sprintf('%02d',$minutes);
	}

	private function renderColorCodes($name, $dark = true)
	{
	    if(empty($name))
	    {
	        return "None found";
	    }
	    $name = htmlspecialchars($name);
	    if(preg_match('/^(#[0-9a-fA-F]{6})+$/', $name) === 1)
	    {
	        return $name;
	    }
	    preg_match_all('/#[0-9a-fA-F]{6}/', $name, $codes);
	    $replaced = array();
	    $codes_original = $codes;
	    $i = 0;
	    $count = 1;
	    $darkify = $dark ? 127 : 0;
	    foreach($codes[0] as &$code)
	    {
	        if(in_array($codes_original[0][$i], $replaced))
	        {
	            continue;
	        }
	        $code = sprintf('%02s', dechex((hexdec($code[1].$code[2])/255*128 + $darkify)))
	            .sprintf('%02s', dechex((hexdec($code[3].$code[4])/255*128 + $darkify)))
	            .sprintf('%02s', dechex((hexdec($code[5].$code[6])/255*128 + $darkify)));
	        $name = str_replace($codes_original[0][$i], "<span style=\"color: #$code;\">", $name, $count);
	        $replaced[] = $codes_original[0][$i];
	        $i++;
	        $count = 1;
	    }
	    while($i > 0)
	    {
	        $name .= "</span>";
	        $i--;
	    }
	    return $name;
	}

}
