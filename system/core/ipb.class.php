<?php
/**



**/

require_once CORE_DIR.'database.class.php';
require_once ROOT_PATH.'/system/utils/rss.class.php';
require_once ROOT_PATH.'/system/core/storage.class.php';

class IPB extends lastRSS{

  private $dbHandle;
  private $forumID;
  public $userName;
  public $isLoggedin;
  private $teamID;
  private $teamMembers;
  public $storage;

  public function __construct(){

    $this->runDatabase();
    $this->forumID = 8;
    $this->teamID = array(7,12,15,25,35,32,33);
    $this->adminIDs = array(12,32,33);
  }

  private function runDatabase(){

    $this->dbHandle = new Database(
      "mysql:host=;dbname=",
      "",
      ""
    );
    $this->checkPlayerLoggedIn();
    $this->storage = new Storage();
  }

  public function loadNews(){

    $this->cache_dir = ROOT_PATH.'/cache';
    $this->cache_time = 600;
    $this->CDATA = 'content';

    $feed = $this->Get('https://forum.twisted-gamers.net/xml/8/') or false;
    if($feed != false){
      $show = array();
      $i = 0;
      $limit = 5;
      foreach($feed["items"] as $index => $content){
        if($i < $limit){
          $show[] = $content;
          $i++;
        }
      }
      $i = 0;
      $news = array();
      foreach($show as $row){
        $news[$i] = array(
          "title" => $row["title"],
          "start_date" => str_replace('+0000', '', $row["pubDate"]),
          "content" => $row["description"]
        );
        $i++;
      }
      return $news;
    }

    /**  $query = "SELECT tid,title,start_date FROM forums_topics WHERE forum_id = ? ORDER by tid DESC LIMIT 5";
      $result = $this->dbHandle->querySafe($query,array( $this->forumID));
      if($result){
        $return = array();
        for($i = 0; $i < count($result); $i++){
          $query = "SELECT * FROM forums_posts WHERE topic_id = ?";
          $qh = $this->dbHandle->querySafe($query,array($result[$i]["tid"]));
          $return[$i] = array(
            "title" => $result[$i]["title"],
            "start_date" => $result[$i]["start_date"],
            "content" => $qh["post"]
          );
        }
      }
     return $return; **/
    }

  private function checkPlayerLoggedIn(){

    /** version 4.2
    if(isset($_COOKIE["ips4_member_id"]) && isset($_COOKIE["ips4_pass_hash"]) && isset($_COOKIE["ips4_login_key"]) && isset($_COOKIE["ips4_device_key"])){
      if(strlen($_COOKIE["ips4_pass_hash"]) > 0){
        var_dump($_COOKIE);
        //GETTING LOGIN KEY FROM `core_members_known_devices` since IPB 4.2
        $query = "SELECT member_id, device_key FROM core_members_known_devices WHERE login_key = ?";
        $stmt = $this->dbHandle->querySafe($query, array($_COOKIE["ips4_login_key"]));
        if($stmt and count($stmt) > 0){
          //Lets get the user Name and compare member_id and device_key //
          if(($stmt[0]["member_id"] == $_COOKIE["ips4_member_id"]) && ($stmt[0]["device_key"] == $_COOKIE["ips4_device_key"])){
            //SUCCES in COMPARISON//
            $userData = $this->dbHandle->querySafe("SELECT name FROM core_members WHERE member_id = ?", array($_COOKIE["ips4_member_id"]));
            $this->userName = $userData[0]["name"];
            $this->isLoggedin = true;
          }else{
            $this->isLoggedin = false;
          }
        }else{
          $this->isLoggedin = false;
        }
      }
    }else{
      $this->isLoggedin = false;
    } **/

    if(isset($_COOKIE["ips4_member_id"]) && isset($_COOKIE["ips4_pass_hash"])){
      if(strlen($_COOKIE["ips4_pass_hash"]) > 0){
        $userData = $this->dbHandle->querySafe("SELECT name FROM core_members WHERE member_id = ?", array($_COOKIE["ips4_member_id"]));
        $this->userName = $userData[0]["name"];
        $this->isLoggedin = true;
      }
    }else{
      $this->isLoggedin = false;
    }
  }

  public function getHashPW($user){
    $stmt = $this->dbHandle->querySafe("SELECT member_pass_hash FROM core_members WHERE name = ?", array($user));
    if($stmt and count($stmt) > 0){
      return $stmt[0]["member_pass_hash"];
    }
  }

  public function getLoginState(){
    if ($this->isLoggedin){

      $query =  "SELECT member_group_id FROM core_members WHERE name = ?";
      $stmt = $this->dbHandle->querySafe($query, array($this->userName));
      $stmt["member_group_id"] = (int)($stmt[0]["member_group_id"]);
      $inteam = (in_array($stmt["member_group_id"], $this->teamID)) ? true: false;
      $isadmin = (in_array($stmt["member_group_id"], $this->adminIDs)) ? true : false;

      return array(
        "name" => $this->userName,
        "userid" => $_COOKIE["ips4_member_id"],
        "loggedin" => $this->isLoggedin,
        "inteam" => $inteam,
        "isadmin" => $isadmin
      );
    }else{
      return array(
        "loggedin" => false
      );
    }
  }

  public function loginPlayer($username,$password){

    $query = "SELECT member_id, name, members_pass_hash, members_pass_salt FROM core_members WHERE name = ?";
    $stmt = $this->dbHandle->querySafe($query,array($username));
    if($stmt){
      $stmt = $stmt[0];
      //Blowfish script//
      if ( mb_strlen ($stmt["members_pass_salt"]) == 22){
        $compare = crypt( $password, "$2a$13$" . $stmt["members_pass_salt"]);
      }else{
        $compare = strtolower(md5(strtolower(md5($stmt["members_pass_salt"]))) . strtolower(md5($password)));
      }

      if($compare == $stmt["members_pass_hash"]){
        if(!isset($_COOKIE["ips4_member_id"])){
          setcookie("ips4_member_id", $stmt["member_id"], time()+60*60*24*30);
          setcookie("ips4_member_name", $stmt["name"], time()+60*60*24*30);
          setcookie("ips4_pass_hash", $stmt["members_pass_hash"], time()+60*60*24*30);
        }
        $this->userName = $stmt["name"];
        $this->isLoggedin = true;
        return true;
      }else{
        return false;
      }
    }
  }

  public function logoutPlayer(){
    if(isset($_COOKIE["ips4_pass_hash"])){
      unset($_COOKIE["ips4_member_id"]);
      unset($_COOKIE["ips4_member_name"]);
      unset($_COOKIE["ips4_pass_hash"]);
      setcookie("ips4_member_id", null, time()-3600);
      setcookie("ips4_member_name", null, time()-3600);
      setcookie("ips4_pass_hash", null, time()-3600);
      $params = session_get_cookie_params();
      setcookie(session_name(), '', 0, $params['path'], $params['domain'], $params['secure'], isset($params['httponly']));
      session_cache_expire();
      return true;
    }
    return false;
  }

  public function loadTeamMembers(){
    $this->dbHandle->executeSafe("USE tgmtaniq_ipboard", array());
    $query =  "SELECT name, member_id, pp_main_photo, member_group_id, mgroup_others FROM `core_members` WHERE `mgroup_others` LIKE '%,7,%' ORDER BY member_group_id ASC, leaderSort ASC";
    $stmt = $this->dbHandle->querySafe($query,$this->teamID);
    $teams = array(
      7 => "<span class='status' style='background:#DB933B;'>Member<span />",
      12 => "<span class='status' style='background:#5E2DC2;'>Developer<span />",
      15 => "<span class='status' style='background:#DB933B;'>Trial<span />",
      25 => "<span class='status' style='background:#43B5B5;'>Forum Manager<span />",
      35 => "<span class='status' style='background:#63A83B;'>Community Manager<span />",
      32 => "<span class='status' style='background:#FC3D3D;'>Co-Leader<span  />",
      33 => "<span class='status' style='background:#FC3D3D;'>Leader/Founder<span />"
    );
    $this->teamMembers = array();
    for($i = 0; $i < count($stmt); $i++){
      $result = $this->storage->loadTeamData($stmt[$i]["name"]);
      $this->teamMembers[] = array(
        "group_id" => (int)($stmt[$i]["member_group_id"]),
        "userid" => (int)($stmt[$i]["member_id"]),
        "name" => $stmt[$i]["name"],
        "title" => $teams[$stmt[$i]["member_group_id"]],
        "avatar" => "https://forum.twisted-gamers.net/uploads/".$stmt[$i]["pp_main_photo"],
        "country" => $this->countryFix($result["lastCountry"]),
        "online" => $result["isOnline"],
        "mtanick" => $this->renderColorCodes($result["lastNick"])
      );
    }
    $order = array(33,32,35,12,25,7,15);
    usort($this->teamMembers, function($a, $b) use ($order){
      $pos_a = array_search($a["group_id"], $order);
      $pos_b = array_search($b["group_id"], $order);
      return $pos_a - $pos_b;
    });
    return $this->teamMembers;
  }

  private function countryFix($a){
    switch ($a){
      case 'Mexico':
        return 'mexicoa';
        break;
      default:
        return $a;
        break;
    }
  }

  public function getLatestDonators(){
    $query = "SELECT username, amount, type, date FROM donations ORDER BY ID DESC LIMIT 10";
    $this->dbHandle->executeSafe("USE tgmtaniq_paybysms", array());
    $ret = $this->dbHandle->querySafe($query,array());
    $return = array();
    if($ret){
      for($i = 0; $i < count($ret); $i++){
        $return[] = array(
          "name" => $ret[$i]["username"],
          "amount" => $ret[$i]["amount"],
          "type" => $ret[$i]["type"],
          "date" => date("Y-m-d", $ret[$i]["date"])
        );
      }
    }
    $this->getMonthlyDonationGoal();
    return $return;
  }

  public function getMonthlyDonationGoal(){
    $this->dbHandle->executeSafe("USE tgmtaniq_paybysms", array());
    $timestring = date('Y-m').'-01 00:00:00';
    $check = strtotime($timestring);
    $query = "SELECT amount, type FROM donations WHERE date > ? ORDER BY ID DESC";
    $ret = $this->dbHandle->querySafe($query, array($check));
    $amount = 0;
    $beforeFees = 0;
    if($ret){
      for($i = 0; $i < count($ret) ; $i++){
        $num = floatval($ret[$i]["amount"]);
        if($ret[$i]["type"] == "SMS") {
          $num = number_format($num*0.4, 2);
        }
        $amount = $amount + $num;
        $beforeFees =$beforeFees + floatval($ret[$i]["amount"]);
      }
    }
    return array("goal" => 60, "current" => $amount, "beforeFees" => $beforeFees, "progress" => ($amount/60)*100);
  }

  private function renderColorCodes($name, $dark = true)
	{
	    if(empty($name))
	    {
	        return "None found";
	    }
	    $name = htmlspecialchars($name);
	    if(preg_match('/^(#[0-9a-fA-F]{6})+$/', $name) === 1)
	    {
	        return $name;
	    }
	    preg_match_all('/#[0-9a-fA-F]{6}/', $name, $codes);
	    $replaced = array();
	    $codes_original = $codes;
	    $i = 0;
	    $count = 1;
	    $darkify = $dark ? 127 : 0;
	    foreach($codes[0] as &$code)
	    {
	        if(in_array($codes_original[0][$i], $replaced))
	        {
	            continue;
	        }
	        $code = sprintf('%02s', dechex((hexdec($code[1].$code[2])/255*128 + $darkify)))
	            .sprintf('%02s', dechex((hexdec($code[3].$code[4])/255*128 + $darkify)))
	            .sprintf('%02s', dechex((hexdec($code[5].$code[6])/255*128 + $darkify)));
	        $name = str_replace($codes_original[0][$i], "<span style=\"color: #$code;\">", $name, $count);
	        $replaced[] = $codes_original[0][$i];
	        $i++;
	        $count = 1;
	    }
	    while($i > 0)
	    {
	        $name .= "</span>";
	        $i--;
	    }
	    return $name;
	}

}
?>
