<?php

if(!defined("FIX_PATH")){
  require_once('system/utils/phpseclib/Net/SSH2.php');
}else{
  require_once(FIX_PATH.'Net/SSH2.php');
}

//TESTING//
define('NET_SSH2_LOGGING', NET_SSH2_LOG_COMPLEX);

class ServerSSH{

    private $ssh;
    private $dir;
    private $screen;
    private $status;
    private $console;

    public function __construct($screen,$dir){
      $this->ssh = new Net_SSH2('ip');
      if($this->ssh->login('ssh_user', 'ssh_password')){
        $this->screen = $screen;
        $this->dir = $dir;
      }else{
        echo $this->ssh->getLog();
      }
      $this->getStatus();
    }

    public function outputBuffer($str){
      $this->status = $str;
    }

    public function outputConsole($str){
     $ret = "";
     if(!empty($str)){
       $ret = preg_replace("/\r\n|\r|\n/", "<br />", $str);
     }
     $this->console = $ret;
    }

    public function getStatus(){
      $this->ssh->exec('screen -wipe');
      $this->ssh->exec('/home/servers/MTA/'.$this->dir.'/check.sh', array($this, 'outputBuffer'));
      return $this->status;
    }

    public function getConsole(){
      $this->ssh->exec('screen -wipe');
      $this->ssh->exec('tail -n 100 /home/servers/MTA/'.$this->dir.'/mods/deathmatch/logs/server.log', array($this, 'outputConsole'));
      return $this->console;
    }


}

?>
