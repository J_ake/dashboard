<?php
/**



**/

require_once 'database.class.php';

class Clans{

  private $dbChatHandle;
  private $dbHandle;

  public function __construct(){
    $this->runDatabase();
  }

  private function runDatabase(){

    $this->$dbChatHandle = new Database(
      "mysql:host=;dbname=",
      "",
      ""
    );
  }

  public function getPlayerClan($name){
  }

  private function renderColorCodes($name, $dark = true)
	{
	    if(empty($name))
	    {
	        return "None found";
	    }
	    $name = htmlspecialchars($name);
	    if(preg_match('/^(#[0-9a-fA-F]{6})+$/', $name) === 1)
	    {
	        return $name;
	    }
	    preg_match_all('/#[0-9a-fA-F]{6}/', $name, $codes);
	    $replaced = array();
	    $codes_original = $codes;
	    $i = 0;
	    $count = 1;
	    $darkify = $dark ? 127 : 0;
	    foreach($codes[0] as &$code)
	    {
	        if(in_array($codes_original[0][$i], $replaced))
	        {
	            continue;
	        }
	        $code = sprintf('%02s', dechex((hexdec($code[1].$code[2])/255*128 + $darkify)))
	            .sprintf('%02s', dechex((hexdec($code[3].$code[4])/255*128 + $darkify)))
	            .sprintf('%02s', dechex((hexdec($code[5].$code[6])/255*128 + $darkify)));
	        $name = str_replace($codes_original[0][$i], "<span style=\"color: #$code;\">", $name, $count);
	        $replaced[] = $codes_original[0][$i];
	        $i++;
	        $count = 1;
	    }
	    while($i > 0)
	    {
	        $name .= "</span>";
	        $i--;
	    }
	    return $name;
	}

}
?>
