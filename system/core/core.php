<?php
/**
  @Copyright Twisted-Gamers & Jake
  @Version 0.0.1 - alpha
  @Author Jake
**/

define('CORE_DIR', dirname(__FILE__).'/');

require_once CORE_DIR.'database.class.php';
require_once ROOT_PATH.'/system/utils/viewer.php';
require_once ROOT_PATH.'/system/dwoo/vendor/autoload.php';
require_once CORE_DIR.'ipb.class.php';

class Core{

  private $database;
  private $stylesheets;
  private $template;
  private $tmplDirectory;
  public  $viewer;
  public $ipb;

  public function __construct(){
      $this->runDatabase();
      $this->runTemplateEngine();
      $this->runViewer();
      $this->runIPB();
      $this->stylesheets = [];
  }

  public function __destruct(){}

  public function __loadEssentials(){}

    /** Database **/
  private function runDatabase(){
    $this->database = New Database("mysql:host=;dbname=","","");
    $this->databse = $this->database->getConnection();
  }

  public function execDB($statement,$args){
    return $this->database->executeSafe($statement,$args);
  }

  public function queryDB($statement,$args){
    return $this->database->querySafe($statement,$args);
  }

  /** template System **/
  private function runTemplateEngine(){
    $this->template = new Dwoo\Core();
    $this->tmplDirectory = ROOT_PATH.'/system/templates/';
  }

  public function loadTemplate($dataSet,$tpl){
    if($this->template){
      return $this->template->get($this->tmplDirectory.$tpl,$dataSet);
    }
  }

  public function addCSS($url){
    $this->stylesheets[] = $url;
    return "<link rel='stylesheet' type='text/css' href='".$url."' />";
  }

  public function addJS($url){
    return "<script src='".$url."' type='text/javascript'></script>";
  }

  public function loadPage($page){
    require_once(ROOT_PATH.'/system/pages/'.$page.'.php');
  }

  /** server Viewer **/
  private function runViewer(){
    $this->viewer = new Viewer();
  }

  public function getMTAStatus(){
    return $this->viewer->getMTAStatus();
  }

  public function getDiscordStatus(){
    return $this->viewer->getDiscordData();
  }

  /** IPB **/
  private function runIPB(){
    $this->ipb = new IPB();
  }


  public function getUploadedMaps(){
    $query = "SELECT username, userid, mapresource, state, comment, tester, date FROM mapuploader ORDER BY mapid DESC LIMIT 25";
    $ret = $this->queryDB($query, array());
    return $ret;
  }
}
?>
