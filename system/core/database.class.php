<?php

  class Database{

    private $dbh;

    public function __construct($host,$user,$pass){
        $db = NULL;
          try{
            $db = new PDO(
              $host,
              $user,
              $pass
            );
          } catch( PDOException $e){
              echo "Unable to built up connection: ".$e->getMessage();
          }
          $this->dbh = $db;
    }

    public function getConnection(){
      return $this->dbh;
    }

    public function executeSafe($statement, $args){
      $query = $this->dbh->prepare($statement);
      if($args == NULL){
        $return = $query->execute();
      }else{
        for($i = 0; $i < count($args); $i++){
          $args[$i] = $this->sanitize($args[$i]);
        }
        $return = $query->execute($args);
      }
      return $return;
    }

    public function querySafe($statement, $args = NULL){
      $query = $this->dbh->prepare($statement);
      if($args == NULL){
        $query->execute();
      }else{
        for($i = 0; $i < count($args); $i++){
          $args[$i] = $this->sanitize($args[$i]);
        }
        $query->execute($args);
      }
      if($query){
        $result = $query->fetchAll(PDO::FETCH_CLASS);
        $result = json_decode(json_encode($result), True);
        return $result;
      }
      return false;
    }

    private function sanitize($val){
        $val = trim(htmlentities(strip_tags($val)));
        if(get_magic_quotes_gpc()){
          $val = stripslashes($val);
        }
        return $val;
    }


  }


?>
