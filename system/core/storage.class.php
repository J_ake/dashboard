<?php

  require_once ROOT_PATH.'/system/core/database.class.php';

  class Storage{

    private $dbHandle;

    public function __construct(){

      $this->dbHandle = new Database(
        "mysql:host=;dbname=",
        "",
        ""
      );

    }


    public function loadTopTimesOld($serial){

      $query = "SELECT mapName, name, time, position, date FROM toptimes WHERE serial = ? ORDER by position";
      $stmt = $this->dbHandle->querySafe($query,array($serial));
      if($stmt){
        for($i = 0; $i < count($stmt); $i++){
          $input = floor(intval($stmt[$i]["time"])/1000);
          $seconds = $input % 60;
          $input = floor($input / 60);

          $minutes = $input % 60;
          $input = floor($input / 60);
          $stmt[$i]["time"] = sprintf('%02d',$minutes).':'.sprintf('%02d',$seconds)
          ;
        }

        return $stmt;
      }
      return false;
    }

    public function loadTeamData($accname){

      $query = "SELECT isOnline, lastNick, lastCountry FROM storage WHERE account = ? LIMIT 1";
      $stmt = $this->dbHandle->querySafe($query,array($accname));
      if($stmt){
        return $stmt[0];
      }
      return false;
    }

    public function loadUserDataOld($accname){

      $query = "SELECT * FROM storage WHERE account = ?";
      $stmt = $this->dbHandle->querySafe($query,array($accname));
      if($stmt){
        $stmt = $stmt[0];
        // Money formatin //
        $displayData = array(
          "Cash" => "Total cash",
          "Exp" => "Total exp",
          "SP" => "Total skillpoints earned",
          "TotalSP" => "Total skillpoints",
          "Wins" => "Total Wins",
          "Attempts" => "Maps played",
          "DDWins" => "DD Wins",
          "DDAttempts" => "DD maps Played",
          "DDPercent" => "DD winrate",
          "DMWins" => "DM wins",
          "DMAttempts" => "DM maps played",
          "DMPercent" => "DM winrate",
          "RACEWins" => "Race wins",
          "RACEAttempts" => "Race maps played",
          "RACEPercent" => "Race winrate",
          "CTFAttempts" => "CTF maps played",
          "CTFWins" => "CTF wins",
          "CTFPercent" => "CTF winrate",
          "CTFFlagsTaken" => "CTF flags taken",
          "CTFFlagsCaptured" => "CTF flags captured",
          "CTFFlagsReturned" => "CTF flags returned",
          "SHAttempts" => "Shooter maps played",
          "SHWins" => "Shooter wins",
          "SHPercent" => "Shooter winrate",
          "TimesTaken" => "Hunters taken",
          "HunterPercent" => "Hunters ratio",
          "Winstreak" => "Highest win streak",
          "Playtime" => "Playtime",
          "Achievements" => "Achievements unlocked",
          "TasksCompleted" => "Tasks completed",
          "Toptimes" => "Toptimes",
          "Joined" => "Times joined the server",
          "RocketsFired" => "Rockets fired",
          "KilledHunter" => "Kills with hunter",
          "MapsBought" => "Total maps bought",
          "PVPWins" => "PVP wins"
        );
        $ret = array();
        foreach($stmt as $key => $value){
          if (array_key_exists($key, $displayData) ){
            if(strpos($key,"Percent")){
              $value = $value.'%';
            }
            if($key == "Playtime"){
              $hours = intval($value/60);
              $minutes = intval($value) - floor($hours*60);
              $value = sprintf('%02d', $hours).':'.sprintf('%02d', $minutes);
            }
            if ($key == "Cash"){
              $value = '$ '.number_format(intval($value),0,',','.');
            }
            $ret[$displayData[$key]] = $value;
          }
        }


        return $ret;
      }
      return false;

    }

    public function getPlayerSerial($accname){

      $query = "SELECT lastSerial,lastNick FROM storage WHERE account = ? LIMIT 1";
      $stmt = $this->dbHandle->querySafe($query,array($accname));
      return $stmt[0];

    }

}
