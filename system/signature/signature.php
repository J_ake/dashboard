<?php
  /**
  @Author: Johannes 'jake' Horngacher
  @Copyright: Twisted Gamers
  @URL: http://twisted-gamers.net
  **/

  require_once 'system/core/ipb.class.php'; //for user-id stuff
  require_once 'system/core/mtauser.class.php'; //for accountdata stuff

  function explodeStringByColorCodes($str) {
        preg_match('/^[^#]+/', $str, $m);

        preg_match_all('/(#[0-9a-fA-F]{6})([^#]+)/', $str, $matches);

        if(count($m) == 1) {
            array_unshift($matches[1], '#FFFFFF');
            array_unshift($matches[2], $m[0]);
        }

        $results = array();

        for($k = 0; $k < count($matches[1]); $k++) {
            $results[$k][0] = $matches[2][$k];
            $results[$k][1] = $matches[1][$k];
        }

    return $results;
  }

  function hex2rgb($hex) {
    $hex = str_replace("#", "", $hex);

      if(strlen($hex) == 3) {
          $r = hexdec(substr($hex,0,1).substr($hex,0,1));
          $g = hexdec(substr($hex,1,1).substr($hex,1,1));
          $b = hexdec(substr($hex,2,1).substr($hex,2,1));
      } else {
         $r = hexdec(substr($hex,0,2));
          $g = hexdec(substr($hex,2,2));
          $b = hexdec(substr($hex,4,2));
      }
      $rgb = array($r, $g, $b);
      return $rgb; // returns an array with the rgb values
  }

  function formatMoneySpecial($money){
    $money = str_replace('$ ', '', $money);
    $money = str_replace('.','', $money);
    $len = strlen($money);
    switch($len){
      case ($len <= 3):
        return $money;
        break;
      case (($len > 3) && ($len <= 6)):
        $money = substr($money,0,2).' K';
        break;
      case (($len > 6) && ($len <= 9)):
        $money = substr($money,0,2).' MIL';
        break;
      case ($len > 9):
        $money = substr($money,0,$len-9).' BN';
        break;
    }
    return $money;
  }

  function createImage($name,$data,$id){
    $path = 'system/signature/bg/';
    $font = 'system/signature/font.ttf';
    $writepath = 'system/signature/signatures/'.$name.'/'.$id.'.png';
    $img = imagecreatefrompng($path.$id.'.png');
    $white = imagecolorallocate($img, 255, 255, 255);
    $black = imagecolorallocate($img, 0, 0, 0);
    $orange = imagecolorallocate($img, 255, 131, 19);
    $black_alpha = imagecolorallocatealpha($img, 0, 0, 0, 50);
    $recw = 70;
    // Draw Username //
    $x = 5;
    $y = 19;
    imagettftext($img, 13, 0, $x+6, $y+6, $black, $font, $name);
    imagettftext($img, 13, 0, $x+5, $y+5, $white, $font, $name);

    $logo = imagecreatefrompng($path.'logo-transp.png');
    $lx = imagesx($logo);
    $ly = imagesy($logo);
    imagecopyresampled($img, $logo, 320, 5, 0, 0, $lx*0.4, $ly*0.4, $lx, $ly);

    if($data){
      // time //
      $x = $x + 5;
      $ystart = 40;
      $yend = 95;
      imagefilledrectangle($img, $x, $ystart, $x+$recw, $yend, $black_alpha);
      $time = imagecreatefrompng($path.'time.png');
      imagecopyresampled($img, $time, $x+22, 65, 0, 0, 24, 24, 128, 128);
      $box = imagettfbbox(10, 0, $font, $data["Playtime"]);
      $w = $box[2] - $box[0];
      imagettftext($img, 10, 0, ($x+35)-($w/2), 57, $white, $font, $data["Playtime"]);

      $x = $x + $recw + 15;
      $money = formatMoneySpecial($data["Money"]);
      imagefilledrectangle($img, $x, $ystart, $x+$recw, $yend, $black_alpha);
      $time = imagecreatefrompng($path.'money.png');
      imagecopyresampled($img, $time, $x+22, 65, 0, 0, 24, 24, 128, 128);
      $box = imagettfbbox(10, 0, $font, $money);
      $w = $box[2] - $box[0];
      imagettftext($img, 10, 0, ($x+35)-($w/2), 57, $white, $font, $money);

      $x = $x + $recw + 15;
      imagefilledrectangle($img, $x, $ystart, $x+$recw, $yend, $black_alpha);
      $time = imagecreatefrompng($path.'map.png');
      imagecopyresampled($img, $time, $x+23, 65, 0, 0, 24, 24, 128, 128);
      $box = imagettfbbox(10, 0, $font, $data["Total maps played"]);
      $w = $box[2] - $box[0];
      imagettftext($img, 10, 0, ($x+35)-($w/2), 57, $white, $font, $data["Total maps played"]);

      $x = $x + $recw + 15;
      imagefilledrectangle($img, $x, $ystart, $x+$recw, $yend, $black_alpha);
      $time = imagecreatefrompng($path.'wins.png');
      imagecopyresampled($img, $time, $x+23, 65, 0, 0, 24, 24, 128, 128);
      $box = imagettfbbox(10, 0, $font, $data["Maps won"]);
      $w = $box[2] - $box[0];
      imagettftext($img, 10, 0, ($x+35)-($w/2), 57, $white, $font, $data["Maps won"]);
    }

    //BODY
    imagefilledrectangle($img, 0, 105, 350, 120, $black_alpha);
    //Online State//
    $green = imagecolorallocate($img, 156, 240, 72);
    $red = imagecolorallocate($img, 255, 0, 0);
    imagettftext($img, 8, 0, 10, 117, $white, $font, "Server is");
    imagettftext($img, 8, 0, 57, 117, $green, $font, "Online");
    imagettftext($img, 8, 0, 200, 116, $orange, $font, "https://twisted-gamers.net");


    /**$y = $y + 25;
    imagettftext($img, 10, 0, $x+1, $y+1, $black, $font, "Playtime:".$data["Playtime"]);
    imagettftext($img, 10, 0, $x, $y, $white, $font, "Playtime:".$data["Playtime"]);

    $y = $y + 19;
    imagettftext($img, 10, 0, $x+1, $y+1, $black, $font, "Money:".$data["Money"]);
    imagettftext($img, 10, 0, $x, $y, $white, $font, "Money:".$data["Money"]);

    $y = $y + 19;
    imagettftext($img, 10, 0, $x+1, $y+1, $black, $font, $data["Total maps played"]." maps played");
    imagettftext($img, 10, 0, $x, $y, $white, $font, $data["Total maps played"]." maps played");

    $y = $y + 19;
    imagettftext($img, 10, 0, $x+1, $y+1, $black, $font, $data["Maps won"]." maps won");
    imagettftext($img, 10, 0, $x, $y, $white, $font, $data["Maps won"]." maps won"); **/


    ob_start();
    imagepng($img);
    $handle = ob_get_contents();
    ob_end_clean();
    //write image file//
    if( !file_exists('system/signature/signatures/'.$name.'/')){ mkdir('system/signature/signatures/'.$name.'/', 0755); }
    $f = fopen($writepath,"wb");
    fwrite($f, $handle);
    fclose($f);
  }


  function newSignature($username, $id){
        $mta = new MTAUser();
        $ipb = new IPB();
        //$account = $mta->getAccountData($_GET["username"],$ipb->getHashPW($_GET["username"]));
        $account = $mta->getAccountData('jake','1234'); //local testing only
        if($account){
          createImage($username,$account["stats"],$id);
        }else{
          createImage($username,false,$id);
        }
    }

?>
